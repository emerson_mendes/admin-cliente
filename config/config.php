<?php
// ERROR REPORTING
error_reporting(E_ALL & ~E_NOTICE);

if (DIRECTORY_SEPARATOR == '/')
    $absolute_path = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR;
else
    $absolute_path = str_replace('\\', '/', dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR);

// DATABASE SETTINGS
define('DB_HOST', 'localhost');
define('DB_NAME', 'test');
define('DB_CHARSET', 'utf8');
define('DB_USER', 'root');
define('DB_PASSWORD', '');
define('BASE_PATH', $absolute_path);
define('BASE_ROOT', $_SERVER['DOCUMENT_ROOT']);
define('BASE_HOST', "http://" . $_SERVER['HTTP_HOST'] );
define('APP_PATH', BASE_ROOT . "/");

// FILE PATHS
define("PATH_CONFIG", BASE_PATH . "config" . DIRECTORY_SEPARATOR);

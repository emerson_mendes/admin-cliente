<?php

session_start();
require 'vendor/autoload.php';
require_once './config/config.php';

require './app/Core/routes.php';

if (isset($_GET['error'])) {
    echo '<div style="position: absolute;top: 0;text-align: center;padding: 10px 0;width: 100%;color: red;">' . $_GET['error'] . '</div>';
}

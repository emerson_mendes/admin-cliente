

$('.header__navbar').on('click', function () {
  if (!$('body').hasClass('sidebar-mini')) {
    $('body').addClass('sidebar-mini');
  } else {
    $('body').removeClass('sidebar-mini');
  }
});

var i = 2;

$('.btn__add--address').on('click', function () {
  $('.form__address:last').append('<div class="form__address">' +
    '<div><button type="button" class="btn__remove--address" onclick="remove_address(this);"><i class="far fa-trash-alt"></i></button><h4>Endereço ' + i +
    '</h4></div><div class="form__input  col-sm-6">' +
    '<label for="description__' + i + '">Endereço</label>' +
    '<input type="text" placeholder="Endereço" name="description__' + i + '" required>' +
    '</div>' +
    '<div class="form__input col-sm-2">' +
    '<label for="number__' + i + '">Numero</label>' +
    '<input type="text" placeholder="Numero" name="number__' + i + '" required>' +
    '</div>' +
    '<div class="form__input col-sm-2">' +
    '<label for="city__' + i + '">Cidade</label>' +
    '<input type="text" placeholder="Cidade" name="city__' + i + '" required>' +
    '</div>' +
    '<div class="form__input col-sm-2">' +
    '<label for="state__' + i + '">Estado</label>' +
    '<input type="text" placeholder="Estado" name="state__' + i + '" required>' +
    '</div></div>');
  i++;
});

function remove_address(e) {
  e.parentNode.parentNode.remove();
}
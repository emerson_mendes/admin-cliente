<?php

namespace App\Controllers;

use \App\Core\Controller;
use \App\Core\View;
use \App\Models\Users;

class LoginController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title'] = 'LOGIN';

        View::render('login', $data);
    }

    public function login()
    {
        $userLogin = new Users();
        $isLogin = $userLogin->login($_POST['email'], $_POST['password']);
        if ($isLogin) {
            header('Location: /');
        } else {
            header('Location: /login?error=Login e/ou senha incorretos');
        }
    }

    public function logout()
    {
        $_SESSION = array();
        session_destroy();

        header('Location: /login');
    }
}

<?php

namespace App\Controllers;

use \App\Core\Controller;
use \App\Core\View;

class IndexController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $data['title'] = 'INDEX';

        View::renderTemplate('header');
        View::renderTemplate('navbar');
        View::renderTemplate('sidebar');
        View::renderTemplate('main');
        View::render('index', $data);
        View::renderTemplate('footer');
    }
}

<?php

namespace App\Controllers;

use \App\Core\Controller;
use \App\Core\View;
use \App\Models\Clients;
use \App\Models\Address;

class ClientesController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {

        $clients = new Clients();

        $data = $clients->getAllClients();

        View::renderTemplate('header');
        View::renderTemplate('navbar');
        View::renderTemplate('sidebar');
        View::renderTemplate('main');
        View::render('clientes', $data);
        View::renderTemplate('footer');
    }

    public function store()
    {

        $clients = new Clients();
        $clients->setName($_POST['name']);
        $clients->setBirthday($_POST['birthday']);
        $clients->setCpf($_POST['cpf']);
        $clients->setRg($_POST['rg']);
        $clients->setPhone($_POST['phone']);

        $listAddress = array_filter($_POST, function ($k) {
            return strpos($k, '__') !== false;
        }, ARRAY_FILTER_USE_KEY);

        $address = new Address();
        $address->setDescription($_POST['description']);
        $address->setNumber($_POST['number']);
        $address->setCity($_POST['city']);
        $address->setState($_POST['state']);
        $clients->setAddress($address);
       
        $list = array();
        $i = 0;
        $j = 0;
        foreach ($listAddress as $key => $value) {

            if (substr($key, 0, strrpos($key, '__')) === 'description') {
                array_push($list, array(substr($key, 0, strrpos($key, '__')) => $value));
                if ($j > 0) {
                    $i++;
                }
                $j++;
            } else {
                $index = substr($key, 0, strrpos($key, '__'));
                $list[$i][$index] = $value;
            }
        }

        foreach ($list as $key => $value) {
            $address = new Address();
            $address->setDescription($value['description']);
            $address->setNumber($value['number']);
            $address->setCity($value['city']);
            $address->setState($value['state']);
            $clients->setAddress($address);
        }

        $clients->saveClient($clients);
        header('Location: /clientes');
    }

    public function create()
    {
        View::renderTemplate('header');
        View::renderTemplate('navbar');
        View::renderTemplate('sidebar');
        View::renderTemplate('main');
        View::render('clientes-create');
        View::renderTemplate('footer');
    }

    public function edit($id)
    {

        $clients = new Clients();
        $data = $clients->getOneClients($id);

        View::renderTemplate('header');
        View::renderTemplate('navbar');
        View::renderTemplate('sidebar');
        View::renderTemplate('main');
        View::render('clientes-edit', $data);
        View::renderTemplate('footer');
    }

    public function update($id)
    {
        $_PUT = array();
        parse_str(file_get_contents('php://input'), $_PUT);

        $listAddress = array_filter($_PUT, function ($k) {
            return strpos($k, '__') !== false;
        }, ARRAY_FILTER_USE_KEY);


        $list = array();
        $i = 0;
        $j = 0;
        foreach ($listAddress as $key => $value) {

            if (substr($key, 0, strrpos($key, '__')) === 'id') {
                array_push($list, array(substr($key, 0, strrpos($key, '__')) => $value));
                if ($j > 0) {
                    $i++;
                }
                $j++;
            } else {
                $index = substr($key, 0, strrpos($key, '__'));
                $list[$i][$index] = $value;
            }
        }

        $clients = new Clients();
        $clients->setId($id);
        $clients->setName($_PUT['name']);
        $clients->setBirthday($_PUT['birthday']);
        $clients->setCpf($_PUT['cpf']);
        $clients->setRg($_PUT['rg']);
        $clients->setPhone($_PUT['phone']);

        foreach ($list as $key => $value) {
            $address = new Address();
            $address->setId($value['id']);
            $address->setDescription($value['description']);
            $address->setNumber($value['number']);
            $address->setCity($value['city']);
            $address->setState($value['state']);
            $clients->setAddress($address);
        }

        $clients->updateClient($clients);
        header('Location: /clientes');
    }

    public function delete($id)
    {
        $clients = new Clients();
        $clients->deleteClient($id);
        header('Location: /clientes');
    }
}

<?php

namespace App\Controllers;

use \App\Core\Controller;
use \App\Core\View;

class ErrorController extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function notFound()
    {
        $data['error'] = 'Page Not Found';

        View::render('/error/404', $data);
    }
}

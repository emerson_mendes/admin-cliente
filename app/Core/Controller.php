<?php

namespace  App\Core;

use \App\Core\View;

abstract class Controller
{

    public $view;

    public function __construct()
    {
        $this->view = new View();
    }
}

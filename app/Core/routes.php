  
<?php

$router = new \App\Core\Router();
$router->setNamespace('\App\Controllers');
$router->set404('ErrorController@notFound');
$router->get('/', 'IndexController@index');
$router->get('login', 'LoginController@index');
$router->post('login', 'LoginController@login');
$router->get('logout', 'LoginController@logout');

$router->get('clientes', 'ClientesController@index');
$router->post('clientes', 'ClientesController@store');
$router->get('clientes/create', 'ClientesController@create');
$router->get('clientes/{id}/edit', 'ClientesController@edit');
$router->put('clientes/{id}', 'ClientesController@update');
$router->delete('clientes/{id}', 'ClientesController@delete');


$router->run();

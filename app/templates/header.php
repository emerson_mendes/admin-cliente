<?php
session_start();
$uri = isset($_SERVER['REQUEST_URI']) ? str_replace('/', '', explode('?', $_SERVER['REQUEST_URI'])[0]) : '';
if (empty($_SESSION['logged_in']) && $_SESSION['logged_in'] !== TRUE) {
    if ($uri !== 'login') {
        header('Location: login');
        exit;
    }
} else {
    if ($uri === 'login') {
        header('Location: /');
        exit;
    }
}

?>

<!doctype html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <title>ADMIN CLIENT</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_HOST; ?>/assets/css/bootstrap-grid.css">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_HOST; ?>/assets/css/main.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="icon" type="image/x-icon" href="#">
</head>

<body>
    <div class="grid-container">
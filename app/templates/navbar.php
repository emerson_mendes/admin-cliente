<header class="navbar">
    <div class="header__navbar">
        <i class="fas fa-bars"></i>
    </div>
    <div class="header__navbar--logout">
        <a href="/logout">
            <i class="fas fa-power-off"></i>
        </a>
    </div>
</header>
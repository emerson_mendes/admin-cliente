<aside class="sidenav">
    <div class="sidenav__logo">
        ADMIN
    </div>
    <ul class="sidenav__list">
        <li class="sidenav__list-item">
            <a href="/">
                <i class="fas fa-tachometer-alt"></i>
                <p>Dashboard</p>
            </a>
        </li>
        <li class="sidenav__list-item">
            <a href="/clientes">
                <i class="fas fa-th-list"></i>
                <p>Clientes</p>
            </a>
        </li>
    </ul>
</aside>
<div class="card">
    <div class="card__header">CLIENTES EDIT</div>
    <div class="card__body">
        <form action="clientes/<?php echo $data['client']["id"]; ?>" method="post">
            <input name="_method" type="hidden" value="PUT">
            <div class="container">
                <div class="form__input col-sm-12">
                    <label for="name">Nome</label>
                    <input type="text" placeholder="Nome" name="name" value="<?php echo $data['client']["name"]; ?>" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="birthday">Data Nascimento</label>
                    <input type="text" placeholder="Data Nascimento" name="birthday" value="<?php echo $data['client']["birthday"]; ?>" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="cpf">CPF</label>
                    <input type="text" placeholder="CPF" name="cpf" value="<?php echo $data['client']["cpf"]; ?>" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="rg">RG</label>
                    <input type="text" placeholder="RG" name="rg" value="<?php echo $data['client']["rg"]; ?>" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="phone">Telefone</label>
                    <input type="text" placeholder="Telefone" name="phone" value="<?php echo $data['client']["phone"]; ?>" required>
                </div>
                <?php foreach ($data["addresses"] as $key => $address) { ?>
                    <input type="hidden" name="id__<?php echo $key; ?>" value="<?php echo $address["id"]; ?>">
                    <div class="form__input col-sm-6">
                        <label for="description__<?php echo $key; ?>">Endereço</label>
                        <input type="text" placeholder="Endereço" name="description__<?php echo $key; ?>" value="<?php echo $address["description"]; ?>" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="number__<?php echo $key; ?>">Numero</label>
                        <input type="text" placeholder="Numero" name="number__<?php echo $key; ?>" value="<?php echo $address["number"]; ?>" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="city__<?php echo $key; ?>">Cidade</label>
                        <input type="text" placeholder="Cidade" name="city__<?php echo $key; ?>" value="<?php echo $address["city"]; ?>" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="state__<?php echo $key; ?>">Estado</label>
                        <input type="text" placeholder="Estado" name="state__<?php echo $key; ?>" value="<?php echo $address["state"]; ?>" required>
                    </div>
                <?php } ?>
                <div class="form_footer">
                    <button type="submit">Salvar</button>
                    <a href="clientes" class="btn btn__back">Voltar</a>
                </div>
            </div>
        </form>
    </div>
</div>
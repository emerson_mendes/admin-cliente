<!doctype html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <title>ADMIN CLIENT</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_HOST; ?>/assets/css/main.css">
    <link rel="icon" type="image/x-icon" href="#">
</head>

<body>
    <div class="page__notfound">
        <h1>PAGE NOT FOUND</h1>
        <a href="/" class="btn btn__back">Voltar</a>
    </div>
</body>

</html>
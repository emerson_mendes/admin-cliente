<div class="card">
    <div class="card__header">
        CLIENTES
        <a href="clientes/create" class="btn__create"><i class="fas fa-plus"></i></a>
    </div>
    <div class="card__body">
        <table>
            <thead>
                <tr>
                    <th>ID</th>
                    <th>NOME</th>
                    <th>NASCIMENTO</th>
                    <th>CPF</th>
                    <th>RG</th>
                    <th>TELEFONE</th>
                    <th>ACTIONS</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($data)) {
                    foreach ($data as $key => $value) {
                        ?>
                        <tr>
                            <td><?php echo $value['id']; ?></td>
                            <td><?php echo $value['name']; ?></td>
                            <td><?php echo $value['birthday']; ?></td>
                            <td><?php echo $value['cpf']; ?></td>
                            <td><?php echo $value['rg']; ?></td>
                            <td><?php echo $value['phone']; ?></td>
                            <td>
                                <a href="/clientes/<?php echo $value['id']; ?>/edit">
                                    <i class="far fa-edit"></i>
                                </a>
                                <form action="clientes/<?php echo $value['id']; ?>" method="post">
                                    <input name="_method" type="hidden" value="DELETE">
                                    <button type="submit" class="btn__submit">
                                        <i class="far fa-trash-alt"></i>
                                    </button>
                                </form>
                            </td>
                        </tr>
                    <?php }
                    } else { ?>
                    <tr>
                        <td colspan="7" class="table__empty">NENHUM DADO</td>
                    </tr>
                <?php
                } ?>
            </tbody>
        </table>
    </div>
</div>
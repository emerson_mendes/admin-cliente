<div class="card">
    <div class="card__header">CLIENTES CREATE</div>
    <div class="card__body">
        <form action="clientes" method="post">
            <div class="container">
                <div class="form__input col-sm-12">
                    <label for="nome">Nome</label>
                    <input type="text" placeholder="Nome" name="name" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="birthday">Data Nascimento</label>
                    <input type="text" placeholder="Data Nascimento" name="birthday" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="cpf">CPF</label>
                    <input type="text" placeholder="CPF" name="cpf" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="rg">RG</label>
                    <input type="text" placeholder="RG" name="rg" required>
                </div>
                <div class="form__input col-sm-3">
                    <label for="phone">Telefone</label>
                    <input type="text" placeholder="Telefone" name="phone" required>
                </div>
                <div class="form__address">
                    <div>
                        <h4>Endereço 1</h4>
                    </div>
                    <div class="form__input col-sm-6">
                        <label for="description">Endereço</label>
                        <input type="text" placeholder="Endereço" name="description" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="number">Numero</label>
                        <input type="text" placeholder="Numero" name="number" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="city">Cidade</label>
                        <input type="text" placeholder="Cidade" name="city" required>
                    </div>
                    <div class="form__input col-sm-2">
                        <label for="state">Estado</label>
                        <input type="text" placeholder="Estado" name="state" required>
                    </div>
                </div>
                <div class="form_footer">
                    <button type="submit">Salvar</button>
                    <a href="clientes" class="btn btn__back">Voltar</a>
                    <button type="button" class="btn__add--address">ADD ENDEREÇO</button>
                </div>
            </div>
        </form>
    </div>
</div>
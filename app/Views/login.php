<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>ADMIN CLIENT</title>
    <base href="/">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="<?php echo BASE_HOST; ?>/assets/css/style.css">
    <link rel="icon" type="image/x-icon" href="#">
</head>

<body>
    <div class="content">
        <div class="login">
            <header>LOGIN</header>
            <form action="login" method="post">
                <div class="container">
                    <div class="form__input">
                        <label for="email">Email</label>
                        <input type="email" placeholder="Email" name="email" required>
                    </div>
                    <div class="form__input">
                        <label for="password">Password</label>
                        <input type="password" placeholder="Password" name="password" required>
                    </div>
                    <button type="submit">Login</button>
                </div>
            </form>
        </div>
    </div>

</body>

</html>
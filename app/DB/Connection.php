<?php

namespace App\DB;

class Connection
{

    private $dsn = "mysql:host=127.0.0.1;dbname=sistema;charset=utf8";
    private $user = 'root';
    private $pass = '';

    protected $pdo = null;

    private $options = [
        \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
        \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
        \PDO::ATTR_EMULATE_PREPARES   => false
    ];

    public function openConnection()
    {
        try {
            $this->pdo = new \PDO($this->dsn, $this->user, $this->pass, $this->options);
            return $this->pdo;
        } catch (\PDOException $e) {
            throw new \PDOException($e->getMessage(), (int) $e->getCode());
        }
    }

    public function closeConnection()
    {
        $this->con = null;
    }
}
